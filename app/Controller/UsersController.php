<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
public $components = array('Paginator', 'RequestHandler');
    public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow('add','index');
		$this->response->header('Access-Control-Allow-Origin', '*');
		$this->Session->destroy();
       // $this->Auth->allow('add', 'login'); // We can remove this line after we're finished
    }
  
    public function login() {
		
        if ($this->Session->read('Auth.User')) {
        //debug($this->Session->read('Auth.User'));
		//die;
            $this->set(array(
                'message' => array(
                    'text' => __('You are logged in!'),
                    'type' => 'error',
					'status'=> 200,
					'success'=>1
                ),
                '_serialize' => array('message')
            ));
			$this->set(array(
                    'user' => $this->Session->read('Auth.User'),
                    '_serialize' => array('user')
                ));
        }

        if ($this->request->is('post')) {
			//debug($this->request-data);
			//die;
            if ($this->Auth->login()) {
            //debug('if**********************************');
			//die;
			$this->Session->setFlash('secssuful login');
                $this->set(array(
				   'message' => array(
                    'text' => __('You are logged in!'),
                    'type' => 'error',
					'status'=> 200,
					'success'=>1
                ),
                    'user' => $this->Session->read('Auth.User'),
                    '_serialize' => array('user','message')
                ));
				//debug('after 1');
				//die;
				 $this->redirect(array('controller' => 'users','action' => 'add'));
            } else {
				//debug('else *************************');
				//die;
			$this->Session->setFlash('erreur plz try again');
                $this->set(array(
                    'message' => array(
                    'text' => __('You are logged in!'),
                    'type' => 'secuss',
					'status'=> 200,
					'success'=>0
                ),
                '_serialize' => array('message')
                ));
			
               // $this->response->statusCode(401);
            }
        }
    }

    public function logout() {
        if ($this->Auth->logout()) {
            $this->set(array(
                'message' => array(
                    'text' => __('Logout successfully'),
                    'type' => 'info'
                ),
                '_serialize' => array('message')
            ));
        }
    }
	public function isAuthorized($user) {
 // All registered users can add posts
 if ($this->action === 'add') {
     return true;
 }

 // The owner of a post can edit and delete it
 if (in_array($this->action, array('edit', 'delete'))) {
     $postId = $this->request->params['pass'][0];
     if ($this->Post->isOwnedBy($postId, $user['id'])) {
  return true;
     }
 }

 return parent::isAuthorized($user);
}
/*
/**
 * index method
 *
 * @return void

	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void

	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void

	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				return $this->flash(__('The user has been saved.'), array('action' => 'index'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void

	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				return $this->flash(__('The user has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void

	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			return $this->flash(__('The user has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The user could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
*/
  

   
//        $username = env('PHP_AUTH_USER');
//        $pass = env('PHP_AUTH_PW');

        //echo "userpass",  $username, $pass;




    /**
     * index method
     *
     * @return void
     */
  /*  public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }*/
	public function index() {
        $users = $this->User->find('all');
        $this->set(array(
            'users' => $users,
            '_serialize' => array('users')
        ));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
		
		
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->set(array(
                    'message' => array(
                        'text' => __('Registered successfully'),
                        'type' => 'info'
                    ),
                    '_serialize' => array('message')
                ));
            } else {
                $this->set(array(
                    'message' => array(
                        'text' => __('The user could not be saved. Please, try again.'),
                        'type' => 'error'
                    ),
                    '_serialize' => array('message')
                ));
                $this->response->statusCode(400);
            }
        }
    }

}
